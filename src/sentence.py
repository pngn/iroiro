import os

from dictionary import build_dictionary, lookup_readings_and_accent_codes
from accents import accent_type, pitch_classes, remove_duplicates
from segmentation import lookup_segmentation, mecab_enabled
from rendering import pitch_accent_markup

## if executing the file directly we are running the unit tests
#    - file resources are in the build directory
#    - the file is run from the src directory rather than the build directory
#    - build a relative path to modify imports so that they still work
#    - requires to run with __name__ as the src module to support relative imports
relative_path = os.path.join('..', 'build')# if __name__ == "src" else ''
directory_path = os.path.dirname(os.path.normpath(__file__))

dictionary_path = os.path.join(directory_path, relative_path, 'db.csv')
card_stylesheet_path = os.path.join(directory_path, relative_path, 'card.css')

# read files from the disk into memory once on application start up
accent_dictionary = {}
with open(dictionary_path, encoding='utf-8') as csv_file:
    accent_dictionary = build_dictionary(csv_file)

# Converts a readings with their accent codes to the corresponding html markup
#   - receives a list of tuples (reading, accent_code)
#   e.g. markup_for_readings_and_accent_codes([('タベモノ', '0120')]) returns 
def markup_for_readings_and_accent_codes(readings_and_accent_codes):
    return remove_duplicates([pitch_accent_markup(pitch_classes((reading, accent_code)), accent_type(accent_code)) for reading, accent_code in readings_and_accent_codes])

def lookup_and_render_term(term, reading):
    readings_and_accent_codes = lookup_readings_and_accent_codes(accent_dictionary, term)
    # only show the readings that match the results from segmentation
    applicable_readings_and_accent_codes = filter(lambda found_reading_and_accent_code: found_reading_and_accent_code[0] == reading, readings_and_accent_codes)

    markups = markup_for_readings_and_accent_codes(readings_and_accent_codes)

    # if a pronunciation can't be found render a reading anyway
    #  - otherwise render all found readings for one term separated by a ／
    return "／".join(markups) if markups else reading

def lookup_and_render_sentence(sentence):
    if not sentence:
        return ""

    print("got sentence:")
    print(sentence)

    segmentations = lookup_segmentation(sentence)
    print("\ngot segmentations:")
    print(segmentations)
    
    renderings = [lookup_and_render_term(term, reading) for (term, reading) in segmentations]
    print("\ngot renderings:")
    print("\n".join(renderings))

    return "\n".join(renderings)

if __name__ == "__main__":
    def assertEqual(a, b):
        if not a == b: raise AssertionError("\n\n\nTest mismatch:\n      got: {0}\n expected: {1}\n\n\n".format(a, b))

    if not mecab_enabled():
        raise AssertionError("Couldn't find mecab, needed for segmentation")
    

    rendered = lookup_and_render_sentence('挨拶をする')
    print("\nrendered:")
    print(rendered)

    ## running as main means that we only want to run the unit tests for this file
    #   - importing anki modules outside of the anki runtime will fail
    #   - so stop the execution of the programme early
    quit()

