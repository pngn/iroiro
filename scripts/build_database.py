#!/usr/bin/env python3

import sys
import csv

WORD_INDEX = 6
KANJI_INDEX = 7
READING_INDEX = 15
ACCENT_INDEX = 18

if __name__ != "__main__":
    quit();

reader = csv.reader(sys.stdin)
writer = csv.writer(sys.stdout)
for row in reader:
    word = row[WORD_INDEX]
    kanji = row[KANJI_INDEX]
    reading = row[READING_INDEX]
    accent = row[ACCENT_INDEX].rjust(len(reading), '0')
    writer.writerow([word, reading, accent])
    if (word != kanji):
        writer.writerow([kanji, reading, accent])

